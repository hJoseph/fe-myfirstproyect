import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class InputService {

  private list: string[];
  constructor() {
    this.list = [];
  }

  public pushTodo(value: string): void {
    this.list.push(value);
  }
  public get todoList(): string[] {
    return this.list;
  }
  public set todoList(todoList: string[]) {
    this.list = todoList;
  }

}
