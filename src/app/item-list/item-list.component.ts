import { Component, OnInit } from '@angular/core';
import {InputService} from '../input/input.service';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent implements OnInit {

  public listItem: string[];

  constructor(private inputService: InputService) {
    this.listItem = [];
  }

  ngOnInit() {
    this.listItem = this.inputService.todoList;
  }

}
